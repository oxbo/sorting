import os
import cv2
import shutil
from PIL import Image
from imagehash import dhash

# Define the threshold for similarity
similarity_threshold = 10  # You can adjust this value based on your needs

# Get the directory where the script is located
script_directory = os.path.dirname(__file__)

# Create a directory to store similar images
similar_output_directory = os.path.join(script_directory, 'similar_images')

# Create a directory to store unique images
unique_output_directory = os.path.join(script_directory, 'unique_images')

# Function to empty a folder
def empty_folder(folder):
    for filename in os.listdir(folder):
        file_path = os.path.join(folder, filename)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception as e:
            print(e)

# Empty the similar and unique image folders if they are non-empty
if os.path.exists(similar_output_directory):
    empty_folder(similar_output_directory)
if os.path.exists(unique_output_directory):
    empty_folder(unique_output_directory)

# Dictionary to store image hashes and their paths
image_hashes = {}

# Use os.walk to traverse all subdirectories
for folder, subfolders, files in os.walk(script_directory):
    for file in files:
        # Check if the file is an image (you can customize the extensions)
        if file.lower().endswith(('.jpg', '.jpeg', '.png', '.gif', '.bmp')):
            image_path = os.path.join(folder, file)
            
            # Load the image
            image = cv2.imread(image_path)
            
            # Convert the NumPy array to a PIL image object
            pil_image = Image.fromarray(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
            
            # Calculate the dHash for the image
            image_hash = dhash(pil_image, hash_size=8)
            
            # Check if this hash is similar to any previously seen hashes
            similar = False
            for existing_hash, existing_path in image_hashes.items():
                hamming_distance = image_hash - existing_hash
                if hamming_distance < similarity_threshold:
                    similar = True
                    print(f"Similar Image: {image_path} is similar to {existing_path}")
                    if not os.path.exists(similar_output_directory):
                        os.makedirs(similar_output_directory)
                    destination_path = os.path.join(similar_output_directory, os.path.basename(image_path))
                    shutil.copy2(image_path, destination_path)
                    break

            # If not similar to any previously seen images, add it to the dictionary and copy it to the unique folder
            if not similar:
                image_hashes[image_hash] = image_path
                print(f"Unique Image: {image_path}")
                if not os.path.exists(unique_output_directory):
                    os.makedirs(unique_output_directory)
                destination_path = os.path.join(unique_output_directory, os.path.basename(image_path))
                shutil.copy2(image_path, destination_path)

# You can adjust the 'similarity_threshold', 'similar_output_directory', and 'unique_output_directory' as needed