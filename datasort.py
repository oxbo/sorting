import os
import cv2
import shutil

# Define the threshold for darkness detection
darkness_threshold = 18  # You can adjust this value based on your needs

# Define the threshold for blur detection
blur_threshold = 100  # You can adjust this value based on your needs

# Get the directory where the script is located
script_directory = os.path.dirname(__file__)

# Create directories to store the blurry, non-blurry, and dark images
blurry_output_directory = os.path.join(script_directory, 'blurry_images')
non_blurry_output_directory = os.path.join(script_directory, 'non_blurry_images')
dark_output_directory = os.path.join(script_directory, 'dark_images')

# Function to empty a folder
def empty_folder(folder):
    for filename in os.listdir(folder):
        file_path = os.path.join(folder, filename)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception as e:
            print(e)

# Empty the blurry, non-blurry, and dark image folders if they are non-empty
if os.path.exists(blurry_output_directory):
    empty_folder(blurry_output_directory)
if os.path.exists(non_blurry_output_directory):
    empty_folder(non_blurry_output_directory)
if os.path.exists(dark_output_directory):
    empty_folder(dark_output_directory)

# Use os.walk to traverse all subdirectories
for folder, subfolders, files in os.walk(script_directory):
    for file in files:
        # Check if the file is an image (you can customize the extensions)
        if file.lower().endswith(('.jpg', '.jpeg', '.png', '.gif', '.bmp')):
            image_path = os.path.join(folder, file)
            
            # Load the image
            image = cv2.imread(image_path)
            
            # Calculate the mean pixel value to detect darkness
            brightness = cv2.mean(image)[0]
            
            # Calculate the variance of Laplacian to detect blur
            variance = cv2.Laplacian(image, cv2.CV_64F).var()
            
            # Create the output directory based on image quality (dark, blurry, or non-dark and non-blurry)
            if brightness < darkness_threshold:
                # Copy the dark image to the dark images folder
                print(f"Dark Image: {image_path} - Brightness: {brightness}")
                if not os.path.exists(dark_output_directory):
                    os.makedirs(dark_output_directory)
                destination_path = os.path.join(dark_output_directory, os.path.basename(image_path))
                # Check if the source and destination paths are different
                if image_path != destination_path:
                    shutil.copy2(image_path, destination_path)
            elif variance < blur_threshold:
                # Copy the blurry image to the blurry images folder
                print(f"Blurry Image: {image_path} - Variance: {variance}")
                if not os.path.exists(blurry_output_directory):
                    os.makedirs(blurry_output_directory)
                destination_path = os.path.join(blurry_output_directory, os.path.basename(image_path))
                # Check if the source and destination paths are different
                if image_path != destination_path:
                    shutil.copy2(image_path, destination_path)
            else:
                # Copy the non-dark and non-blurry image to the non-blurry images folder
                print(f"Non-Dark and Non-Blurry Image: {image_path}")
                if not os.path.exists(non_blurry_output_directory):
                    os.makedirs(non_blurry_output_directory)
                destination_path = os.path.join(non_blurry_output_directory, os.path.basename(image_path))
                # Check if the source and destination paths are different
                if image_path != destination_path:
                    shutil.copy2(image_path, destination_path)

# You can adjust the 'darkness_threshold', 'blur_threshold', 'blurry_output_directory', 'dark_output_directory', and 'non_blurry_output_directory' as needed